export const todoReducer = (initialState = [], action) => {

  console.log("todoReducer: ", action);

  if (action.type === '[TODO] Add Todo') {
    return [...initialState, action.payload];
  }

  if (action.type === '[TODO] Remove Todo') {
    return initialState.filter(todo => todo.id !== action.payload);
  }

  if (action.type === '[TODO] Toggle Todo') {
    return initialState.map(todo => {
      if (todo.id === action.payload) {
        return {
          ...todo,
          done: !todo.done,
        };
      }

      return todo;
    });
  }

  return initialState;
};