import { getHeroeById, getHeroesByOwner } from "../../src/base-pruebas/08-imp-exp";

describe('pruebas en 08-imp-exp', () => {

  test('getHeroeById debe retornar un heroe por id', () => {
    const id = 1;
    const heroe = getHeroeById(id);

    expect(heroe).toEqual({
      id: 1,
      name: 'Batman',
      owner: 'DC'
    });
  });

  test('getHeroeById debe retornar undefined si no existe', () => {
    const id = 100;
    const testHeroe = getHeroeById(id);

    expect(testHeroe).toBe(undefined);
  });

  test('getHeroesByOwner debe retornar un arreglo con los héroes de DC', () => {
    const testOwner = 'DC';
    const testHeroes = getHeroesByOwner(testOwner);

    expect(testHeroes.length).toBe(3);
    expect(testHeroes).toEqual([
      { id: 1, name: 'Batman', owner: 'DC' },
      { id: 3, name: 'Superman', owner: 'DC' },
      { id: 4, name: 'Flash', owner: 'DC' }
    ]);
  });

  test('getHeroesByOwner debe retornar un arreglo con los héroes de Marvel', () => {
    const owner = 'Marvel';
    const testHeroes = getHeroesByOwner(owner);
    console.log(testHeroes);

    expect(testHeroes.length).toBe(2); 
    expect(testHeroes).toEqual([
      { id: 2, name: 'Spiderman', owner: 'Marvel' },
      { id: 5, name: 'Wolverine', owner: 'Marvel' }
    ]);
  });

});