import React from 'react';
import PropTypes from 'prop-types';

export const FirstApp = ({ title, subTitle, name }) => {

  return (
    <>
      <h1 data-testid="test-title"> {title} </h1>
      <h3>{subTitle}</h3>
      <h3>{subTitle}</h3>
      {/* <code>{JSON.stringify(newMessage)}</code> */}
      <p>{ name }</p>
    </>
  );
};

FirstApp.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string,
};

FirstApp.defaultProps = {
  title: 'No title',
  subTitle: 'No hay subtítulo',
};